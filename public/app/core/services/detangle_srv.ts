import _ from 'lodash';
import coreModule from 'app/core/core_module';
import TableModel from '../table_model';

const issueIdColumn = '@issueId';
const pathColumn = '@path';
const titleColumn = '@issueTitle';
const typeColumn = '@issueType';
const authorColumn = '@author';
const issueOwnerColumn = '@issueOwner';
const yearColumn = '@year';
const daysColumn = '@days';
const intervalColumn = '$interval';
const directoryLevelColumn = '@directoryLevel';
const averageText = 'Average';
const maxText = 'Max';

export class DetangleSrv {
  /** @ngInject */
  constructor() {}

  issueIdIndex: number;
  filePathIndex: number;
  authorIndex: number;
  yearIndex: number;
  valueIndex: number;

  dataConvertor(dataList, templateSrv, config, mode = 'timeseries') {
    config = this.getCustomConfig(templateSrv, config);
    dataList = this.dataFilter(dataList, config);
    if (config.diamondPattern) {
      return this.couplingCalculator(dataList, templateSrv, config, mode);
    } else {
      if (config.coupling) {
        return this.dataCalculator(dataList, config, mode);
      } else if (config.qualityEffort) {
        return this.qualityEffortCalculator(dataList, mode);
      } else {
        return dataList;
      }
    }
  }

  getOverview(dataList, templateSrv, config) {
    //config = this.getCustomConfig(templateSrv, config);
    let rows = dataList[0].rows;
    let columns = dataList[0].columns;
    let directoryLevelIndex: number = DetangleSrv.getIndex(directoryLevelColumn, columns);
    let pathIndex: number = DetangleSrv.getIndex(pathColumn, columns);
    // let valueIndex = columns.length - 1;
    rows = rows.filter(item => item[directoryLevelIndex] > 0);
    rows = _.orderBy(rows, pathIndex, 'asc');
    rows = rows.map(item => {
      let newObject = {};
      newObject[0] = item[pathIndex];
      for (let i = 0; i < columns.length; i++) {
        if (i === directoryLevelIndex || i === pathIndex) {
          continue;
        }
        newObject[i - 1] = item[i];
      }
      return newObject;
    });
    dataList[0].rows = rows;
    let newColumns = [];
    for (let i = 0; i < columns.length; i++) {
      if (i === directoryLevelIndex) {
        continue;
      }
      if (columns[i].text === averageText || columns[i].text === maxText) {
        columns[i].text = 'nLOC';
      }
      newColumns.push(columns[i]);
    }
    dataList[0].columns = newColumns;
    return dataList;
  }

  dataCalculator(dataList, config, mode = 'timeseries') {
    let rows = dataList[0].rows;
    let columns = dataList[0].columns;
    let isAuthorDashboard = false;
    let isTemporalDashboard = false;
    this.issueIdIndex = DetangleSrv.getIndex(issueIdColumn, columns);
    if (this.issueIdIndex === -1) {
      this.issueIdIndex = DetangleSrv.getIndex(authorColumn, columns);
      if (this.issueIdIndex === -1) {
        this.issueIdIndex = DetangleSrv.getIndex(intervalColumn, columns);
        isTemporalDashboard = true;
        config.minFilesData = 2;
        config.isTemporalDashboard = true;
      } else {
        isAuthorDashboard = true;
        config.isAuthorDashboard = true;
      }
    }
    //let issueTypeIndex: number = getIndex('@issue_type');
    this.filePathIndex = DetangleSrv.getIndex(pathColumn, columns);
    //let timestampIndex: number = getIndex('@timestamp');
    this.authorIndex = DetangleSrv.getIndex(issueOwnerColumn, columns);
    this.yearIndex = DetangleSrv.getIndex(yearColumn, columns);

    this.valueIndex = columns.length - 1;
    let hasIssueType = false;
    let hasIssueTitle = false;
    let hasOtherMetric = false;
    let issueTypeRows: any = {};
    let issueTitleRows: any = {};
    let otherMetricRows: any = {};
    if (!isAuthorDashboard && !isTemporalDashboard && dataList.length > 1) {
      for (let i = 1; i < dataList.length; i++) {
        let tempColumns = dataList[i].columns;
        let tempIssueTypeIndex: number = DetangleSrv.getIndex(typeColumn, tempColumns);
        let tempIssueTitleIndex: number = DetangleSrv.getIndex(titleColumn, tempColumns);
        let tempSumIndex: number = DetangleSrv.getIndex('Sum', tempColumns);

        if (tempIssueTypeIndex > -1) {
          hasIssueType = true;
          let tempIssueIdIndex: number = DetangleSrv.getIndex(issueIdColumn, tempColumns);
          let tempRows = dataList[i].rows;
          let groupedIssueTypes = DetangleSrv.groupBy(tempRows, tempIssueIdIndex);
          issueTypeRows = Object.keys(groupedIssueTypes).reduce(function(previous, current) {
            previous[current] = groupedIssueTypes[current][0][tempIssueTypeIndex];
            return previous;
          }, {});
        }

        if (tempIssueTitleIndex > -1) {
          hasIssueTitle = true;
          let tempIssueIdIndex: number = DetangleSrv.getIndex(issueIdColumn, tempColumns);
          let tempRows = dataList[i].rows;
          let groupedIssueTitles = DetangleSrv.groupBy(tempRows, tempIssueIdIndex);
          issueTitleRows = Object.keys(groupedIssueTitles).reduce(function(previous, current) {
            previous[current] = groupedIssueTitles[current][0][tempIssueTitleIndex];
            return previous;
          }, {});
        }

        if (tempSumIndex > -1) {
          hasOtherMetric = true;
          let tempIssueIdIndex: number = DetangleSrv.getIndex(issueIdColumn, tempColumns);
          let tempRows = dataList[i].rows;
          let groupedIssueMetric = DetangleSrv.groupBy(tempRows, tempIssueIdIndex);
          otherMetricRows = Object.keys(groupedIssueMetric).reduce(function(previous, current) {
            previous[current] = groupedIssueMetric[current][0][tempSumIndex];
            return previous;
          }, {});
        }
      }
    }
    let checkIssueSourceType = false;
    let checkIssueTargetType = false;
    let issueTypeHash = new Map();
    let targetIssueTypeHash = new Map();

    let checkAuthorSourceType = false;
    let checkAuthorTargetType = false;
    let authorTypeHash = new Map();
    let targetAuthorTypeHash = new Map();

    if (!isAuthorDashboard) {
      if (hasIssueType && config.sourceTypeData !== 'All') {
        checkIssueSourceType = true;
        issueTypeHash = new Map(_.split(config.sourceTypeData, ' + ').map(i => [i, true]));
      }
      if (hasIssueType && config.targetTypeData !== 'All') {
        checkIssueTargetType = true;
        targetIssueTypeHash = new Map(_.split(config.targetTypeData, ' + ').map(i => [i, true]));
      }
    } else {
      if (config.sourceTypeData !== 'All') {
        checkAuthorSourceType = true;
        authorTypeHash = new Map(_.split(config.sourceTypeData, ' + ').map(i => [i, true]));
      }
      if (config.targetTypeData !== 'All') {
        checkAuthorTargetType = true;
        targetAuthorTypeHash = new Map(_.split(config.targetTypeData, ' + ').map(i => [i, true]));
      }
    }
    let yearList = [];
    if (config.yearData && config.yearData !== '' && config.yearData !== 'All') {
      yearList = _.split(config.yearData, ' + ');
      if (config.yearCompare) {
        let startYear = 2000;
        let endYear = _.min(yearList);
        yearList = Array(endYear - startYear)
          .fill(undefined)
          .map((e, i) => i + 2000);
      }
    } else {
      let startYear = 2000;
      let endYear = new Date().getFullYear() + 1;
      yearList = Array(endYear - startYear)
        .fill(undefined)
        .map((e, i) => i + 2000);
    }

    // let t0 = performance.now();

    let isIssueTarget = config.target === 'issue';

    let fileHighlightFilter = '';
    let fileHighlightRegexChecker;
    if (config.fileHighlightData) {
      fileHighlightFilter = config.fileHighlightData.trim();
      fileHighlightRegexChecker = new RegExp(fileHighlightFilter);
    }
    let shouldHighlightFiles =
      fileHighlightFilter !== '' && fileHighlightFilter !== '-' && fileHighlightFilter !== '$file_highlight';

    let metric = '';
    switch (config.metric) {
      case 'couplecounts': {
        metric = 'numOfIssues';
        break;
      }
      case 'cohesion': {
        metric = 'cohesionValue';
        break;
      }
      case 'coupling':
      default: {
        metric = 'couplingValue';
        break;
      }
    }
    let indexToCheck = isIssueTarget ? this.issueIdIndex : this.filePathIndex;
    let isCouplingValue = metric === 'couplingValue';
    let issueTitle = DetangleSrv.trimConfParameter(config.issueTitleData);
    let sourceAcceptedFiles = {};
    let targetAcceptedFiles = {};
    let issueSorceControlList = {};
    let issueTargetControlList = {};
    if (checkIssueSourceType || checkIssueTargetType || checkAuthorSourceType || checkAuthorTargetType) {
      let issueGroupedData = DetangleSrv.groupByArray(rows, this.issueIdIndex);
      for (let i = 0; i < issueGroupedData.length; i++) {
        let tempIssueId = issueGroupedData[i].item;
        if (checkAuthorSourceType || checkIssueSourceType) {
          if (isAuthorDashboard) {
            if (authorTypeHash.get(tempIssueId)) {
              issueSorceControlList[tempIssueId] = true;
            }
          } else {
            if (issueTypeHash.get(issueTypeRows[tempIssueId])) {
              issueSorceControlList[tempIssueId] = true;
            }
          }
        }
        if (checkAuthorTargetType || checkIssueTargetType) {
          if (isAuthorDashboard) {
            if (targetAuthorTypeHash.get(tempIssueId)) {
              issueTargetControlList[tempIssueId] = true;
            }
          } else {
            if (targetIssueTypeHash.get(issueTypeRows[tempIssueId])) {
              issueTargetControlList[tempIssueId] = true;
            }
          }
        }
      }
    }
    let shouldMinIssuesChecked =
      config.minIssuesData &&
      config.minIssuesData !== '-' &&
      config.minIssuesData !== '$min_issues' &&
      config.minIssuesData !== '$min_issues_coh';
    let shouldMinFilesChecked =
      config.minFilesData && config.minFilesData !== '-' && config.minFilesData !== '$min_files';
    let shouldCheckIssueTitle =
      hasIssueTitle && issueTitle !== '' && issueTitle !== '$issue_title' && issueTitle !== '-';
    let issueTitleRegexChecker = new RegExp(issueTitle);
    let shouldValueHashFilled = mode === 'table';
    let shouldMinCoupledChecked = isIssueTarget ? shouldMinFilesChecked : shouldMinIssuesChecked;
    let minEntriesData = isIssueTarget ? config.minFilesData : config.minIssuesData;
    let shouldMinConnectionChecked = isIssueTarget ? shouldMinIssuesChecked : shouldMinFilesChecked;
    let minConnectionData = isIssueTarget ? config.minIssuesData : config.minFilesData;

    let couplingCalculation = (rowsToBeCalculated, onlyCouplingValue) => {
      let issueCouples = new Map();
      // let reverseCouples = new Map();
      let valueHash = new Map();
      let issueSearchControlList = {};
      let valueGroupedData = DetangleSrv.groupBy(
        rowsToBeCalculated,
        isIssueTarget ? this.issueIdIndex : this.filePathIndex
      );
      let valueGroupedArray = Object.keys(valueGroupedData).map(item => ({
        item,
        connections: valueGroupedData[item],
      }));
      let objectToBeCalculated = {};
      for (let index = 0; index < valueGroupedArray.length; index++) {
        let tempItem = valueGroupedArray[index];
        let tempConnections = tempItem.connections;
        let numberOfConnectedObjects = tempConnections.length;

        let tempValueHash = {
          nAll: 0,
          sourceSquare: 0,
          showIt: false,
        };
        if (isIssueTarget && shouldCheckIssueTitle) {
          let tempIssueTitle = issueTitleRows[tempItem.item];
          if (issueTitleRegexChecker.test(tempItem.item + tempIssueTitle)) {
            issueSearchControlList[tempItem.item] = true;
          }
        }
        let numberOfConnectedObjectsCalculated = numberOfConnectedObjects;
        for (let i = 0; i < numberOfConnectedObjects; i++) {
          let tempConnection = tempConnections[i];
          let tempValue = tempConnection[this.valueIndex];
          tempValueHash.nAll += tempValue;
          tempValueHash.sourceSquare += tempValue * tempValue;
          if (!isIssueTarget) {
            if (shouldCheckIssueTitle && !issueSearchControlList[tempItem.item]) {
              let tempIssueTitle = issueTitleRows[tempConnection[this.issueIdIndex]];
              if (issueTitleRegexChecker.test(tempItem.item + tempConnection[this.issueIdIndex] + tempIssueTitle)) {
                issueSearchControlList[tempItem.item] = true;
              }
            }
            if (
              (checkAuthorSourceType || checkIssueSourceType) &&
              issueSorceControlList[tempConnection[this.issueIdIndex]]
            ) {
              sourceAcceptedFiles[tempItem.item] = true;
            }
            if (checkAuthorTargetType || checkIssueTargetType) {
              if (issueTargetControlList[tempConnection[this.issueIdIndex]]) {
                targetAcceptedFiles[tempItem.item] = true;
              } else {
                --numberOfConnectedObjectsCalculated;
              }
            }
          }
        }
        let checkMinEntries = shouldMinCoupledChecked ? minEntriesData : 1;
        if (numberOfConnectedObjectsCalculated >= checkMinEntries) {
          valueHash.set(tempItem.item, tempValueHash);
          objectToBeCalculated[tempItem.item] = true;
        }
      }
      rowsToBeCalculated = rowsToBeCalculated.filter(item => objectToBeCalculated[item[indexToCheck]]);
      let groupedData = DetangleSrv.groupBy(rowsToBeCalculated, isIssueTarget ? this.filePathIndex : this.issueIdIndex);
      let fileObjectArray = Object.keys(groupedData).map(file => ({ file, issues: groupedData[file] }));
      for (let index = 0; index < fileObjectArray.length; index++) {
        let fileObject = fileObjectArray[index];
        let fileObjectIssues = fileObject.issues;
        let fileObjectIssuesLength = fileObjectIssues.length;
        let connectionCheck = fileObjectIssuesLength;
        if (checkIssueTargetType || checkAuthorTargetType) {
          if (isIssueTarget) {
            connectionCheck = fileObjectIssues.filter(item => issueTargetControlList[item[this.issueIdIndex]]).length;
          } else {
            connectionCheck = fileObjectIssues.filter(item => targetAcceptedFiles[item[this.filePathIndex]]).length;
          }
        }
        if (shouldMinConnectionChecked && connectionCheck < minConnectionData) {
          continue;
        }
        for (let i = 0; i < fileObjectIssuesLength; i++) {
          let source = fileObjectIssues[i];
          let sourceValue = source[this.valueIndex];
          if (sourceValue === 0) {
            continue;
          }
          let sourceGroupValue = source[indexToCheck];
          if (checkIssueSourceType || checkAuthorSourceType) {
            if (isIssueTarget && !issueSorceControlList[sourceGroupValue]) {
              continue;
            }
            if (!isIssueTarget && !sourceAcceptedFiles[sourceGroupValue]) {
              continue;
            }
            if (!isIssueTarget && issueSorceControlList[fileObject.file]) {
              let valueIndexHash = valueHash.get(sourceGroupValue);
              valueIndexHash.showIt = true;
            }
          }
          let sourceSquare = sourceValue * sourceValue;
          let sourceCoupleHash = issueCouples.get(sourceGroupValue);
          if (!sourceCoupleHash) {
            issueCouples.set(sourceGroupValue, new Map());
            sourceCoupleHash = issueCouples.get(sourceGroupValue);
          }
          for (let j = 0; j < fileObjectIssuesLength; j++) {
            let target = fileObjectIssues[j];
            let targetValue = target[this.valueIndex];
            if (targetValue === 0) {
              continue;
            }
            let targetGroupValue = target[indexToCheck];
            if (sourceGroupValue === targetGroupValue) {
              continue;
            }

            if (checkIssueTargetType || checkAuthorTargetType) {
              if (isIssueTarget && !issueTargetControlList[targetGroupValue]) {
                continue;
              }
              if (!isIssueTarget && !targetAcceptedFiles[targetGroupValue]) {
                continue;
              }
            }
            let targetSquare = targetValue * targetValue;
            let sourceTargetProduct = sourceValue * targetValue;

            let sourceCoupleHashIssues = sourceCoupleHash;

            let tempTarget = sourceCoupleHashIssues.get(targetGroupValue);
            if (!tempTarget) {
              sourceCoupleHashIssues.set(targetGroupValue, {
                sourceSquare: sourceSquare,
                targetSquare: targetSquare,
                sourceTargetProduct: sourceTargetProduct,
              });
              // sourceCoupleHash.numOfIssues ++;
            } else {
              tempTarget.sourceSquare += sourceSquare;
              tempTarget.targetSquare += targetSquare;
              tempTarget.sourceTargetProduct += sourceTargetProduct;
            }
          }
        }
      }
      // let t1 = performance.now();
      // console.log(issueCouples);
      let issueCouplesArray = [];
      if (mode === 'chord') {
        let threshold = config.threshold === null || !config.threshold ? 0 : config.threshold;

        issueCouples.forEach((value, key) => {
          let valueIndexHash = valueHash.get(key);
          if (shouldCheckIssueTitle && !issueSearchControlList[key]) {
            return;
          }
          if (shouldHighlightFiles && !fileHighlightRegexChecker.test(key)) {
            return;
          }
          if (
            !isIssueTarget &&
            (checkIssueTargetType || checkAuthorTargetType) &&
            (checkIssueSourceType || checkAuthorSourceType) &&
            !valueIndexHash.showIt
          ) {
            return;
          }
          let targetHash = {};
          value.forEach((valueOfCouple, keyOfCouple) => {
            if (onlyCouplingValue) {
              // console.log(key);
              // console.log(keyOfCouple);
              let tempTarget = valueHash.get(keyOfCouple);
              // console.log(valueOfCouple.sourceTargetProduct / ((Math.sqrt(valueIndexHash.sourceSquare * tempTarget.sourceSquare))));
              if (valueIndexHash.sourceSquare === 0 || tempTarget.sourceSquare === 0) {
                return;
              }
              let couplingValue =
                valueOfCouple.sourceTargetProduct / Math.sqrt(valueIndexHash.sourceSquare * tempTarget.sourceSquare);
              if (couplingValue > threshold) {
                let targetObj = targetHash[keyOfCouple];
                if (targetObj) {
                  targetHash[keyOfCouple] += couplingValue;
                } else {
                  targetHash[keyOfCouple] = couplingValue;
                }
              }
              // TODO: Fix here
              // if (shouldGroupFiles && isFileGroupInt) {
              //   if (isIssueTarget) {
              //     coupledObj.couplingValue = coupledObj.couplingValue / Math.sqrt(Math.pow(2, maxFileDepth - fileGroupIndex));
              //   } else {
              //     //console.log(Math.sqrt(Math.pow(2, maxFileDepth - fileGroupIndex)));
              //     coupledObj.couplingValue = coupledObj.couplingValue * Math.sqrt(Math.pow(2, maxFileDepth - fileGroupIndex));
              //   }
              // }
            }
          });
          Object.keys(targetHash).forEach(function(targetKey) {
            let couplingValue = targetHash[targetKey];
            if (couplingValue !== 0) {
              let coupledObj = {
                source: key,
                target: targetKey,
                couplingValue: couplingValue,
              };
              issueCouplesArray.push(coupledObj);
            }
          });
        });
      } else {
        issueCouples.forEach((value, key) => {
          let valueIndexHash = valueHash.get(key);
          if (shouldCheckIssueTitle && !issueSearchControlList[key]) {
            return;
          }
          if (shouldHighlightFiles && !fileHighlightRegexChecker.test(key)) {
            return;
          }
          if (!isIssueTarget && (checkIssueTargetType || checkAuthorTargetType) && !valueIndexHash.showIt) {
            return;
          }
          let coupledObj = {
            id: key,
            couplingValue: 0,
            numOfIssues: 0,
            nAll: valueIndexHash.nAll,
            metric: 0,
          };
          if (shouldValueHashFilled && hasOtherMetric) {
            let otherMetricObj = otherMetricRows[key];
            if (otherMetricObj) {
              coupledObj.metric = otherMetricObj;
            }
          }
          value.forEach((valueOfCouple, keyOfCouple) => {
            coupledObj.numOfIssues++;
            if (onlyCouplingValue) {
              // console.log(key);
              // console.log(keyOfCouple);
              let tempTarget = valueHash.get(keyOfCouple);
              // console.log(valueOfCouple.sourceTargetProduct / ((Math.sqrt(valueIndexHash.sourceSquare * tempTarget.sourceSquare))));
              if (valueIndexHash.sourceSquare === 0 || tempTarget.sourceSquare === 0) {
                return;
              }
              coupledObj.couplingValue +=
                valueOfCouple.sourceTargetProduct / Math.sqrt(valueIndexHash.sourceSquare * tempTarget.sourceSquare);
              // TODO: Fix here
              // if (shouldGroupFiles && isFileGroupInt) {
              //   if (isIssueTarget) {
              //     coupledObj.couplingValue = coupledObj.couplingValue / Math.sqrt(Math.pow(2, maxFileDepth - fileGroupIndex));
              //   } else {
              //     //console.log(Math.sqrt(Math.pow(2, maxFileDepth - fileGroupIndex)));
              //     coupledObj.couplingValue = coupledObj.couplingValue * Math.sqrt(Math.pow(2, maxFileDepth - fileGroupIndex));
              //   }
              // }
            }
          });
          if (onlyCouplingValue && coupledObj.couplingValue === 0) {
            return;
          }
          if (!onlyCouplingValue && coupledObj.numOfIssues === 0) {
            return;
          }
          issueCouplesArray.push(coupledObj);
        });
      }
      return issueCouplesArray;
    };
    let cohesionCalculation = rowsToBeCalculated => {
      let itemCohesions = new Map();
      let itemCohesionArray = [];
      if (isAuthorDashboard) {
        rowsToBeCalculated = rowsToBeCalculated.filter(
          item =>
            !checkAuthorSourceType ||
            authorTypeHash.get(item[this.issueIdIndex]) ||
            (!checkAuthorTargetType || targetAuthorTypeHash.get(item[this.issueIdIndex]))
        );
      } else {
        rowsToBeCalculated = rowsToBeCalculated.filter(
          item =>
            !checkIssueSourceType ||
            issueTypeHash.get(issueTypeRows[item[this.issueIdIndex]]) ||
            (!checkIssueTargetType || targetIssueTypeHash.get(issueTypeRows[item[this.issueIdIndex]]))
        );
      }
      let valueGroupedData = DetangleSrv.groupBy(
        rowsToBeCalculated,
        isIssueTarget ? this.issueIdIndex : this.filePathIndex
      );
      let valueGroupedArray = Object.keys(valueGroupedData).map(item => ({
        item,
        connections: valueGroupedData[item],
      }));
      let objectToBeCalculated = {};
      let issueSearchControlList = {};
      for (let index = 0; index < valueGroupedArray.length; index++) {
        let tempItem = valueGroupedArray[index];
        let tempConnections = tempItem.connections;
        let numberOfConnectedObjects = tempConnections.length;
        if (isIssueTarget && shouldCheckIssueTitle) {
          let tempIssueTitle = issueTitleRows[tempItem.item];
          if (issueTitleRegexChecker.test(tempItem.item + tempIssueTitle)) {
            issueSearchControlList[tempItem.item] = true;
          }
        }
        for (let i = 0; i < numberOfConnectedObjects; i++) {
          let tempConnection = tempConnections[i];
          if (!isIssueTarget) {
            if (shouldCheckIssueTitle && !issueSearchControlList[tempItem.item]) {
              let tempIssueTitle = issueTitleRows[tempConnection[this.issueIdIndex]];
              if (issueTitleRegexChecker.test(tempItem.item + tempIssueTitle)) {
                issueSearchControlList[tempItem.item] = true;
              }
            }
          }
        }
        if (!shouldMinCoupledChecked || numberOfConnectedObjects >= minEntriesData) {
          objectToBeCalculated[tempItem.item] = true;
        }
      }

      if (shouldMinCoupledChecked) {
        rowsToBeCalculated = rowsToBeCalculated.filter(item => objectToBeCalculated[item[indexToCheck]]);
      }
      if (shouldMinConnectionChecked) {
        objectToBeCalculated = {};
        let connectionCheckIndex = isIssueTarget ? this.filePathIndex : this.issueIdIndex;
        let groupedDataReverse = DetangleSrv.groupBy(rowsToBeCalculated, connectionCheckIndex);
        let fileObjectArrayReverse = Object.keys(groupedDataReverse).map(file => ({
          file,
          issues: groupedDataReverse[file],
        }));
        for (let index = 0; index < fileObjectArrayReverse.length; index++) {
          let fileObject = fileObjectArrayReverse[index];
          let fileObjectIssues = fileObject.issues;
          let connectionCheck = fileObjectIssues.length;
          if (checkIssueTargetType || checkAuthorTargetType) {
            if (isIssueTarget) {
              connectionCheck = fileObjectIssues.filter(item => issueTargetControlList[item[this.issueIdIndex]]).length;
            } else {
              connectionCheck = fileObjectIssues.filter(item => targetAcceptedFiles[item[this.filePathIndex]]).length;
            }
          }
          if (shouldMinConnectionChecked && connectionCheck >= minConnectionData) {
            objectToBeCalculated[fileObject.file] = true;
          }
        }
        rowsToBeCalculated = rowsToBeCalculated.filter(item => objectToBeCalculated[item[connectionCheckIndex]]);
      }
      let groupedData = DetangleSrv.groupBy(rowsToBeCalculated, this.filePathIndex);
      let fileObjectArray = Object.keys(groupedData).map(file => ({ file, issues: groupedData[file] }));
      let numberOfOneCohesions = {},
        issueFileCount = {};
      for (let index = 0; index < fileObjectArray.length; index++) {
        let fileObject = fileObjectArray[index];
        let fileObjectIssues = fileObject.issues;
        let fileObjectIssuesLength = fileObjectIssues.length;
        let totalChange = fileObjectIssues.reduce((acc, obj) => {
          return acc + obj[this.valueIndex];
        }, 0);
        if (totalChange === 0) {
          continue;
        }
        let fileCumulative = fileObjectIssues.reduce((acc, obj) => {
          let ratio = obj[this.valueIndex] / totalChange;
          if (config.cohesionCalculationMethod === 'double' || metric === 'couplingValue') {
            ratio *= ratio;
          }
          return acc * (ratio + 1);
        }, 1);
        let maxMetric = 0;
        for (let i = 0; i < fileObjectIssuesLength; i++) {
          let source = fileObjectIssues[i];
          let sourceGroupValue = source[this.issueIdIndex];
          let sourceValue = source[this.valueIndex];
          if (isIssueTarget) {
            let ratio = sourceValue / totalChange;
            if (config.cohesionCalculationMethod === 'double' || metric === 'couplingValue') {
              ratio *= ratio;
            }
            if (issueFileCount[sourceGroupValue]) {
              ++issueFileCount[sourceGroupValue];
            } else {
              issueFileCount[sourceGroupValue] = 1;
            }
            let weight = fileCumulative / (ratio + 1);
            let cohesionValue = ratio / weight;
            let itemCohesion = itemCohesions.get(sourceGroupValue);
            if (!itemCohesion) {
              itemCohesions.set(sourceGroupValue, cohesionValue);
            } else {
              itemCohesions.set(sourceGroupValue, itemCohesion * cohesionValue);
            }
            if (cohesionValue === 1) {
              if (numberOfOneCohesions[sourceGroupValue]) {
                ++numberOfOneCohesions[sourceGroupValue];
              } else {
                numberOfOneCohesions[sourceGroupValue] = 1;
              }
            }
          } else {
            if (maxMetric < sourceValue) {
              maxMetric = sourceValue;
            }
          }
        }
        if (!isIssueTarget) {
          if (shouldCheckIssueTitle && !issueSearchControlList[fileObject.file]) {
            continue;
          }
          if (shouldHighlightFiles && !fileHighlightRegexChecker.test(fileObject.file)) {
            continue;
          }
          let ratio = maxMetric / totalChange;
          if (config.cohesionCalculationMethod === 'double' || metric === 'couplingValue') {
            ratio *= ratio;
          }
          let weight = fileCumulative / (ratio + 1);
          let cohesionValue = ratio / weight;
          let cohesionObj = {
            id: fileObject.file,
            cohesionValue: cohesionValue,
          };
          itemCohesionArray.push(cohesionObj);
        }
      }

      if (isIssueTarget) {
        itemCohesions.forEach((value, key) => {
          if (shouldCheckIssueTitle && !issueSearchControlList[key]) {
            return;
          }
          if (shouldMinFilesChecked && issueFileCount[key] < config.minFilesData) {
            return;
          }
          if (isAuthorDashboard) {
            if (checkAuthorSourceType && !authorTypeHash.get(key)) {
              return;
            }
          } else {
            if (checkIssueSourceType && !issueTypeHash.get(issueTypeRows[key])) {
              return;
            }
          }
          let cohesionObj = {
            id: key,
            cohesionValue: config.cohesionCalculationMethod === 'double' ? Math.sqrt(Math.sqrt(value)) : value,
          };
          cohesionObj.cohesionValue = Math.pow(cohesionObj.cohesionValue, 1 / (1 + (numberOfOneCohesions[key] || 0)));
          itemCohesionArray.push(cohesionObj);
        });
      }
      let folderLevel = DetangleSrv.trimConfParameter(config.folderLevel);
      let shouldApplyFolderLevel = folderLevel !== '' && folderLevel !== '-' && folderLevel !== '$folder_level';
      let isFolderLevelInt = DetangleSrv.isInt(folderLevel);

      if (shouldApplyFolderLevel && config.applyFolderLevel && isFolderLevelInt) {
        let folderLevelIndex = parseInt(folderLevel, 10);
        let pathList = {};
        for (let i = 0; i < itemCohesionArray.length; i++) {
          let tempCouplingObj = itemCohesionArray[i];
          let folder = tempCouplingObj.id.substring(
            0,
            DetangleSrv.nthIndex(tempCouplingObj.id, '/', folderLevelIndex) + 1
          );
          if (!pathList[folder]) {
            pathList[folder] = {
              cohesionValue: tempCouplingObj.cohesionValue,
            };
          } else {
            pathList[folder].cohesionValue += tempCouplingObj.cohesionValue;
          }
        }
        let tempFmiList = [];
        Object.keys(pathList).map(item => {
          let tempValueHash = pathList[item];
          tempFmiList.push({
            id: item,
            cohesionValue: tempValueHash.cohesionValue,
          });
        });
        itemCohesionArray = tempFmiList;
      }
      return itemCohesionArray;
    };
    if (config.percentile || config.isInterval) {
      let intervalHash = new Map();

      if (
        (config.percentile && config.percentileInterval === 'yearly') ||
        (config.isInterval && config.interval === 'yearly')
      ) {
        let yearArray: string[] = [];
        yearList.forEach(tempYear => {
          let dateInMilliseconds = new Date(tempYear, 0, 1).getTime();
          let tempRows: any[];
          tempRows = rows.filter(item => item[this.yearIndex] === tempYear.toString());

          if (config.isAggregated && tempRows.length > 0) {
            yearArray.push(tempYear.toString());
            tempRows = this.removeYearGrouping(rows.filter(item => yearArray.includes(item[this.yearIndex])));
          }
          if (tempRows.length > 0) {
            let tempCouplingArray =
              metric === 'cohesionValue'
                ? cohesionCalculation(tempRows)
                : couplingCalculation(tempRows, isCouplingValue);
            if (config.percentile) {
              tempCouplingArray = tempCouplingArray.sort((a, b) => {
                return a[metric] - b[metric];
              });
            }
            intervalHash.set(dateInMilliseconds, tempCouplingArray);
          }
        });
      }
      let returnList = [];
      let lastPercentileValue = 0;
      if (config.percentile) {
        let percentileValues = config.percentileRanks.split(',').map(Number);
        percentileValues.forEach(percentile => {
          let tempPercentileObject = {
            datapoints: [],
            metric: 'p' + percentile + '.0',
            target: 'p' + percentile + '.0 ' + metric,
            props: {},
            field: metric,
          };
          intervalHash.forEach((value, key) => {
            if (value.length === 0) {
              return;
            }
            let tempPercentileIndex = percentile / 100 * value.length;
            let calculatedPercentile = 0;
            let floorOfIndex = Math.floor(tempPercentileIndex);
            if (floorOfIndex === tempPercentileIndex) {
              calculatedPercentile = (value[tempPercentileIndex][metric] + value[tempPercentileIndex][metric]) / 2;
            } else {
              calculatedPercentile = value[floorOfIndex][metric];
            }
            tempPercentileObject.datapoints.push({
              0: calculatedPercentile,
              1: key,
            });
            lastPercentileValue = calculatedPercentile;
          });
          returnList.push(tempPercentileObject);
        });
      } else {
        intervalHash.forEach((value, key) => {
          let limit = config.limit === null || !config.limit ? value.length : config.limit;
          let tempReturnList = _.map(_.take(_.orderBy(value, metric, config.sortingOrder), limit), issueObject => {
            return {
              props: {},
              target: issueObject.id,
              description: isIssueTarget ? issueTitleRows[issueObject.id] : undefined,
              datapoints: [{ 0: issueObject[metric], 1: key }],
            };
          });
          returnList.push(tempReturnList);
        });
      }
      if (mode === 'singlevalue') {
        lastPercentileValue = +lastPercentileValue.toFixed(3);
        return {
          valueRounded: lastPercentileValue.toString(),
          valueFormatted: lastPercentileValue.toString(),
          value: lastPercentileValue,
          flotpairs: [[0, lastPercentileValue]],
        };
      } else {
        return _.flatten(returnList);
      }
    } else {
      let calculatedArray = [];
      if (metric === 'cohesionValue') {
        calculatedArray = cohesionCalculation(rows);
      } else {
        calculatedArray = couplingCalculation(rows, isCouplingValue);
      }
      // console.log(fileObjectArray);
      //console.log(fileObjectArray);
      // console.log("Call to groupby took " + (t1 - t0) + " milliseconds.");
      // console.log("Call to calculateCouplings took " + (t2 - t1) + " milliseconds.");
      let shouldFilterLocal = config.localFilter && config.localFilter !== '' && !isIssueTarget;
      if (shouldFilterLocal) {
        let regexChecker = new RegExp(config.localFilter);
        calculatedArray = calculatedArray.filter(item => regexChecker.test(item.id));
      }
      if (mode === 'timeseries') {
        let now = _.now();
        let limit = config.limit === null || !config.limit ? calculatedArray.length : config.limit;
        let additionalHash = {};
        if (config.additionalMetric) {
          let additionalArray = isCouplingValue ? cohesionCalculation(rows) : couplingCalculation(rows, true);
          if (shouldFilterLocal) {
            let regexChecker = new RegExp(config.localFilter);
            additionalArray = additionalArray.filter(item => regexChecker.test(item.id));
          }
          additionalHash = additionalArray.reduce(function(map, obj) {
            map[obj.id] = isCouplingValue ? obj.cohesionValue : obj.couplingValue;
            return map;
          }, {});
        }
        let dataPointGenerator = primary => {
          if (config.additionalMetric) {
            if (isCouplingValue) {
              return { 0: primary[metric], 1: now, 2: additionalHash[primary.id] };
            } else {
              return { 0: additionalHash[primary.id], 1: now, 2: primary[metric] };
            }
          } else {
            return { 0: primary[metric], 1: now };
          }
        };
        return _.map(_.take(_.orderBy(calculatedArray, metric, config.sortingOrder), limit), issueObject => {
          return {
            metric: 'sum',
            props: { '@issue_id': issueObject.id },
            target: issueObject.id,
            field: '$metric',
            description: isIssueTarget ? issueTitleRows[issueObject.id] : undefined,
            datapoints: [dataPointGenerator(issueObject)],
          };
        });
      } else if (mode === 'table') {
        let cohesionArray = cohesionCalculation(rows);
        if (shouldFilterLocal) {
          let regexChecker = new RegExp(config.localFilter);
          cohesionArray = cohesionArray.filter(item => regexChecker.test(item.id));
        }
        let cohesionHash = cohesionArray.reduce(function(map, obj) {
          map[obj.id] = obj.cohesionValue;
          return map;
        }, {});

        let tableList = [];
        let valuesToBeAdd = [
          {
            key: 'couplingValue',
            value: 'Coupling Value',
          },
          {
            key: 'numOfIssues',
            value: 'Coupling Neighbours',
          },
          {
            key: 'nAll',
            value: 'nAll',
          },
          {
            key: 'cohesionValue',
            value: 'Cohesion Value',
          },
        ];
        if (hasOtherMetric) {
          valuesToBeAdd.push({
            key: 'metric',
            value: 'metric',
          });
        }
        _.each(valuesToBeAdd, metricObj => {
          let arrayToUse = calculatedArray;
          let tableModel = new TableModel();
          tableModel.columnMap = {};
          tableModel.columns = [];
          tableModel.rows = [];
          tableModel.type = 'table';
          // let tableModel = {
          //   columnMap : {},
          //   columns: [],
          //   rows: [],
          //   type: 'table',
          // };
          let tempTarget = {
            text: '@' + config.target,
            filterable: true,
          };
          let tempMetric = {
            text: metricObj.value,
          };
          let isCohesion = metricObj.key === 'cohesionValue';
          tableModel.columnMap['@' + config.target] = tempTarget;
          tableModel.columnMap[metricObj.key] = tempMetric;
          tableModel.columns.push(tempTarget);
          tableModel.columns.push(tempMetric);
          tableModel.rows = _.map(_.orderBy(arrayToUse, 'id', 'desc'), issueObject => {
            return {
              0: issueObject.id + (isIssueTarget && hasIssueTitle ? ' - ' + issueTitleRows[issueObject.id] : ''),
              1: isCohesion ? cohesionHash[issueObject.id] : issueObject[metricObj.key],
            };
          });
          tableList.push(tableModel);
        });
        return tableList;
      } else if (mode === 'singlevalue') {
        return {
          valueRounded: '20',
          valueFormatted: '20',
          value: 20,
          flotpairs: [[0, 20]],
        };
      } else if (mode === 'chord') {
        let limit = config.limit === null || !config.limit ? 50 : config.limit;
        calculatedArray = _.take(_.orderBy(calculatedArray, metric, 'desc'), limit);
        return calculatedArray;
      } else if (mode === 'array') {
        if (config.cohesionThreshold) {
          calculatedArray = calculatedArray.filter(item => item.cohesionValue <= config.cohesionThreshold);
        }
        return calculatedArray;
      }
    }
  }

  couplingCalculator(dataList, templateSrv, config, mode) {
    let rows = dataList[0].rows;
    let columns = dataList[0].columns;
    let refIndex = this.findRefIndex(columns);
    let filePathIndex = DetangleSrv.getIndex(pathColumn, columns);
    let valueIndex = columns.length - 1;
    let referenceGroupedData = DetangleSrv.groupBy(rows, refIndex);
    let fileGroupedData = DetangleSrv.groupByArray(rows, filePathIndex);
    let hasIssueTitle = false;
    let issueTitleRows: any = {};
    let issueTitle = DetangleSrv.trimConfParameter(config.issueTitleData);

    if (dataList.length > 1) {
      for (let i = 1; i < dataList.length; i++) {
        let tempColumns = dataList[i].columns;
        let tempIssueTitleIndex: number = DetangleSrv.getIndex(titleColumn, tempColumns);
        if (tempIssueTitleIndex > -1) {
          hasIssueTitle = true;
          let tempIssueIdIndex: number = DetangleSrv.getIndex(issueIdColumn, tempColumns);
          let tempRows = dataList[i].rows;
          let groupedIssueTitles = DetangleSrv.groupBy(tempRows, tempIssueIdIndex);
          issueTitleRows = Object.keys(groupedIssueTitles).reduce(function(previous, current) {
            previous[current] = groupedIssueTitles[current][0][tempIssueTitleIndex];
            return previous;
          }, {});
        }
      }
    }
    let shouldCheckIssueTitle =
      hasIssueTitle && issueTitle !== '' && issueTitle !== '$issue_title' && issueTitle !== '-';

    let pathDifferenceLevel = config.pathDifferenceLevel;
    let shouldCheckPathDifferenceLevel = pathDifferenceLevel > 0;
    let minNumberOfCommonRefs = config.minNumberOfCommonRefs;
    let minNumberOfDiffRefs = config.minNumberOfDiffRefs;
    let eachShouldHaveDiff = config.eachShouldHaveDiff;
    let initialRefFilterCount = eachShouldHaveDiff
      ? minNumberOfCommonRefs + minNumberOfDiffRefs
      : minNumberOfCommonRefs;
    let getNthIndex = (word, substring, index) => {
      return word.split(substring, index).join(substring).length;
    };
    let t0 = performance.now();

    let fileGroupHash = DetangleSrv.groupBy(rows, filePathIndex);
    fileGroupedData = fileGroupedData.filter(item => item.connections.length >= initialRefFilterCount);
    let fileGroupMap = new Map();
    fileGroupedData.map(item => fileGroupMap.set(item.item, fileGroupHash[item.item]));

    let fileDifferentHash = {};
    if (shouldCheckPathDifferenceLevel) {
      fileGroupedData.map(item => {
        let indexToComparePath = getNthIndex(item.item, '/', pathDifferenceLevel);
        fileDifferentHash[item.item] = indexToComparePath < 0 ? item.item : item.item.substring(0, indexToComparePath);
      });
    }
    let coupledFiles = [];
    // let calculationFor = 0;
    // let innerFor = 0;

    let sourceAndTargetCouplingValues = [];
    let isChordDiagram = mode === 'chord';
    for (let i = 0; i < fileGroupedData.length; i++) {
      let tempFilePath = fileGroupedData[i].item;
      // console.log("sourceFile", tempFilePath);
      let sourceConnections = fileGroupedData[i].connections;
      let sourceSquare = 0;
      let sourceRefList = Array.from(new Set(sourceConnections.map(o => o[refIndex])));
      let targetFileList = [];
      let refValueHash = {};
      for (let j = 0; j < sourceConnections.length; j++) {
        let sourceValue = sourceConnections[j][valueIndex];
        let refId = sourceConnections[j][refIndex];
        sourceSquare += sourceValue * sourceValue;
        refValueHash[refId] = sourceValue;
        let refHash = referenceGroupedData[refId];
        targetFileList = targetFileList.concat(refHash.map(o => o[filePathIndex]));
      }
      targetFileList = Array.from(new Set(targetFileList));
      if (shouldCheckPathDifferenceLevel) {
        let sourceFileDifferentHash = fileDifferentHash[tempFilePath];
        targetFileList = targetFileList.filter(item => sourceFileDifferentHash !== fileDifferentHash[item]);
      }
      let couplingValue = 0;
      // let tInnerFor0 = performance.now();

      for (let j = 0; j < targetFileList.length; j++) {
        let tempTargetFile = targetFileList[j];
        if (tempTargetFile === tempFilePath) {
          continue;
        }
        let sourceTargetProductSum = 0;
        let targetSquareSum = 0;
        let tempTargetFileHash = fileGroupMap.get(tempTargetFile);
        if (!tempTargetFileHash) {
          continue;
        }
        let targetRefList = new Set();
        for (let l = 0; l < tempTargetFileHash.length; l++) {
          let item = tempTargetFileHash[l][refIndex];
          targetRefList.add(item);
        }
        // let refArray = tempTargetFileHash.map(o => o[refIndex]);
        // let targetRefList = Array.from(new Set(refArray));
        let intersection = sourceRefList.filter(value => targetRefList.has(value));
        let intersectionLength = intersection.length;
        let sourceDiffCount = sourceRefList.length - intersectionLength;
        let targetDiffCount = targetRefList.size - intersectionLength;
        if (intersection.length < minNumberOfCommonRefs) {
          continue;
        }
        if (eachShouldHaveDiff && (sourceDiffCount < minNumberOfDiffRefs || targetDiffCount < minNumberOfDiffRefs)) {
          continue;
        }
        if (!eachShouldHaveDiff && (sourceDiffCount < minNumberOfDiffRefs && targetDiffCount < minNumberOfDiffRefs)) {
          continue;
        }
        // let tCalculationFor0 = performance.now();
        for (let targetIndex = 0; targetIndex < tempTargetFileHash.length; targetIndex++) {
          let tempTargetInstance = tempTargetFileHash[targetIndex];
          let targetRefId = tempTargetInstance[refIndex];
          let targetValue = tempTargetInstance[valueIndex];
          if (intersection.includes(targetRefId)) {
            sourceTargetProductSum += targetValue * refValueHash[targetRefId];
          }
          let targetSquare = targetValue * targetValue;
          targetSquareSum += targetSquare;
        }
        // let tCalculationFor1 = performance.now();
        // calculationFor += tCalculationFor1 - tCalculationFor0;
        let tempCouplingValue = sourceTargetProductSum / (Math.sqrt(sourceSquare) * Math.sqrt(targetSquareSum));
        if (isChordDiagram && tempCouplingValue) {
          sourceAndTargetCouplingValues.push({
            source: tempFilePath,
            target: tempTargetFile,
            couplingValue: tempCouplingValue,
          });
        }
        couplingValue += tempCouplingValue;
      }
      // let tInnerFor1 = performance.now();
      // innerFor += tInnerFor1 - tInnerFor0;

      let coupledObj = {
        id: tempFilePath,
        couplingValue: couplingValue,
      };
      coupledFiles.push(coupledObj);
      // console.log("source file coupling", couplingValue);
    }
    let t1 = performance.now();
    console.log('Calculation is took ' + (t1 - t0) + ' milliseconds.');
    // console.log('calculation for is took ', calculationFor);
    // console.log('Inner for is took ', innerFor);

    if (shouldCheckIssueTitle) {
      let acceptedIssues = {};
      let issueTitleRegexChecker = new RegExp(issueTitle);
      Object.keys(issueTitleRows).map(issueId => {
        if (issueTitleRegexChecker.test(issueId + issueTitleRows[issueId])) {
          acceptedIssues[issueId] = true;
        }
      });
      if (isChordDiagram) {
        sourceAndTargetCouplingValues = sourceAndTargetCouplingValues.filter(coupledFile => {
          let coupledFileHash = fileGroupHash[coupledFile.source];
          return coupledFileHash.some(item => acceptedIssues[item[refIndex]]);
        });
      } else {
        coupledFiles = coupledFiles.filter(coupledFile => {
          let coupledFileHash = fileGroupHash[coupledFile.id];
          return coupledFileHash.some(item => acceptedIssues[item[refIndex]]);
        });
      }
    }

    if (config.maintainabilityIndex) {
      let cohesionList = this.dataCalculator(
        dataList,
        {
          ...config,
          coupling: true,
          diamondPattern: false,
          metric: 'cohesion',
          target: 'file',
          applyFolderLevel: false,
        },
        'array'
      );
      let pathList = {};
      for (let i = 0; i < cohesionList.length; i++) {
        let tempCohesionObj = cohesionList[i];
        pathList[tempCohesionObj.id] = {
          cohesionValue: tempCohesionObj.cohesionValue,
        };
      }
      for (let i = 0; i < coupledFiles.length; i++) {
        let tempCouplingObj = coupledFiles[i];
        if (!pathList[tempCouplingObj.id]) {
          pathList[tempCouplingObj.id] = {
            couplingValue: tempCouplingObj.couplingValue,
          };
        } else {
          pathList[tempCouplingObj.id].couplingValue = tempCouplingObj.couplingValue;
        }
      }
      let tempFmiList = [];
      Object.keys(pathList).map(item => {
        let tempValueHash = pathList[item];
        if (tempValueHash.cohesionValue && tempValueHash.cohesionValue !== 1) {
          let tempCouplingValue = 1;
          if (tempValueHash.couplingValue && tempValueHash.couplingValue !== 0) {
            tempCouplingValue = tempValueHash.couplingValue;
          }
          tempFmiList.push({
            id: item,
            couplingValue: (1 - tempValueHash.cohesionValue) * tempCouplingValue,
          });
        }
      });
      coupledFiles = tempFmiList;
    }

    let folderLevel = DetangleSrv.trimConfParameter(config.folderLevel);
    let shouldApplyFolderLevel = folderLevel !== '' && folderLevel !== '-' && folderLevel !== '$folder_level';
    let isFolderLevelInt = DetangleSrv.isInt(folderLevel);

    if (shouldApplyFolderLevel && config.applyFolderLevel && isFolderLevelInt) {
      let folderLevelIndex = parseInt(folderLevel, 10);
      if (!isChordDiagram) {
        let pathList = {};
        for (let i = 0; i < coupledFiles.length; i++) {
          let tempCouplingObj = coupledFiles[i];
          let folder = tempCouplingObj.id.substring(
            0,
            DetangleSrv.nthIndexAlternative(tempCouplingObj.id, '/', folderLevelIndex) + 1
          );
          if (!pathList[folder]) {
            pathList[folder] = {
              couplingValue: tempCouplingObj.couplingValue,
            };
          } else {
            pathList[folder].couplingValue += tempCouplingObj.couplingValue;
          }
        }
        let tempFmiList = [];
        Object.keys(pathList).map(item => {
          let tempValueHash = pathList[item];
          tempFmiList.push({
            id: item,
            couplingValue: tempValueHash.couplingValue,
          });
        });
        coupledFiles = tempFmiList;
      } else {
        let coupledFilesNew = {};
        for (let i = 0; i < sourceAndTargetCouplingValues.length; i++) {
          let tempCouplingObj = sourceAndTargetCouplingValues[i];
          tempCouplingObj.source = tempCouplingObj.source.substring(
            0,
            DetangleSrv.nthIndexAlternative(tempCouplingObj.source, '/', folderLevelIndex) + 1
          );
          tempCouplingObj.target = tempCouplingObj.target.substring(
            0,
            DetangleSrv.nthIndexAlternative(tempCouplingObj.target, '/', folderLevelIndex) + 1
          );
          if (tempCouplingObj.source === tempCouplingObj.target) {
            continue;
          }
          let tempKey = tempCouplingObj.source + '~~' + tempCouplingObj.target;
          if (!coupledFilesNew[tempKey]) {
            coupledFilesNew[tempKey] = 0;
          }
          coupledFilesNew[tempKey] += tempCouplingObj.couplingValue;
        }
        coupledFiles = [];

        Object.keys(coupledFilesNew).map(item => {
          let tempValue = coupledFilesNew[item];
          let keyArray = item.split('~~');
          coupledFiles.push({
            source: keyArray[0],
            target: keyArray[1],
            couplingValue: tempValue,
          });
        });
      }
    }
    if (config.showPercentageOf) {
      coupledFiles = _.orderBy(coupledFiles.filter(item => item.couplingValue !== 0), 'couplingValue', 'desc');
      let sizeOfFiles = coupledFiles.length;
      let startOfFilter = Math.floor(sizeOfFiles * ((100 - config.showPercentageOf) / 100));
      coupledFiles = coupledFiles.slice(startOfFilter);
    }

    if (mode === 'timeseries') {
      let now = _.now();
      let limit = config.limit === null || !config.limit ? coupledFiles.length : config.limit;
      let dataPointGenerator = primary => {
        return { 0: primary['couplingValue'], 1: now };
      };
      return _.map(_.take(_.orderBy(coupledFiles, 'couplingValue', config.sortingOrder), limit), fileObject => {
        return {
          metric: 'sum',
          props: { '@file_path': fileObject.id },
          target: fileObject.id,
          field: '$metric',
          // description: isIssueTarget ? issueTitleRows[issueObject.id] : undefined,
          datapoints: [dataPointGenerator(fileObject)],
        };
      });
    } else if (mode === 'singlevalue') {
      let sum = 0;
      for (let i = 0; i < coupledFiles.length; i++) {
        sum += parseFloat(coupledFiles[i].couplingValue); //don't forget to add the base
      }
      let sumFixed = sum.toFixed(3);
      // let avg = sum / coupledFiles.length;
      // let avgFixed = +avg.toFixed(3);
      return {
        valueRounded: sumFixed,
        valueFormatted: sumFixed,
        value: sum,
        flotpairs: [[0, sum]],
      };
    } else if (mode === 'table') {
      let tableModel = new TableModel();
      tableModel.columnMap = {};
      tableModel.columns = [];
      tableModel.rows = [];
      tableModel.type = 'table';
      // let tableModel = {
      //   columnMap : {},
      //   columns: [],
      //   rows: [],
      //   type: 'table',
      // };
      let tempTarget = {
        text: '@path',
        filterable: true,
      };
      let tempMetric = {
        text: config.maintainabilityIndex ? 'DebtIndex' : 'CouplingValue',
      };
      tableModel.columnMap['@path'] = tempTarget;
      tableModel.columnMap[tempMetric.text] = tempMetric;
      tableModel.columns.push(tempTarget);
      tableModel.columns.push(tempMetric);
      tableModel.rows = _.map(coupledFiles, item => {
        return {
          0: item.id,
          1: item.couplingValue,
        };
      });
      let tableList = [];
      tableList.push(tableModel);
      return tableList;
    } else if (mode === 'keyValue') {
      return coupledFiles;
    } else if (mode === 'chord') {
      return coupledFiles;
    }
  }

  qualityEffortCalculator(dataList, mode) {
    let refactoringCost = 0.25;
    let potentialSavings = 0.75;
    if (dataList.lenght < 2) {
      return;
    }
    let pathList = {};
    for (let i = 0; i < 2; i++) {
      let tempColumns = dataList[i].columns;
      let tempRows = dataList[i].rows;
      let valueIndex = tempColumns.length - 1;
      for (let rowIndex = 0; rowIndex < tempRows.length; rowIndex++) {
        let tempRow = tempRows[rowIndex];
        let tempPath = tempRow[0];
        let tempValue = tempRow[valueIndex];
        if (!pathList[tempPath]) {
          pathList[tempPath] = {};
        }
        pathList[tempPath]['value' + i] = tempValue * (i === 0 ? refactoringCost : potentialSavings);
      }
    }
    let generatedRowList = [];
    Object.keys(pathList).map(item => {
      let tempValueHash = pathList[item];
      let tempQualityEffort = 0;
      // if (tempValueHash.value0 && tempValueHash.value1 && tempValueHash.value0 !== 0 && tempValueHash.value1 !== 0) {
      //   tempQualityEffort = tempValueHash.value0 - tempValueHash.value1;
      // }
      if (tempValueHash.value0 && tempValueHash.value0 !== 0) {
        tempQualityEffort = tempValueHash.value0;
      }
      generatedRowList.push({
        0: item,
        1: tempQualityEffort,
        2: tempValueHash.value0,
        3: tempValueHash.value1,
      });
    });
    if (mode === 'timeseries') {
      let returnDataList: any = {};
      returnDataList.rows = generatedRowList;
      returnDataList.columns = [
        { text: '@path', filterable: true },
        { text: 'Ratio' },
        { text: 'Metric1' },
        { text: 'Metric2' },
      ];
      returnDataList.columnMap = {
        '@path': { text: '@path', filterable: true },
        Ratio: { text: 'Ratio' },
        Metric1: { text: 'Metric1' },
        Metric2: { text: 'Metric2' },
      };
      returnDataList.type = 'table';
      returnDataList = [returnDataList];
      return returnDataList;
    } else if (mode === 'singlevalue') {
      let sum = 0;
      for (let i = 0; i < generatedRowList.length; i++) {
        sum += parseFloat(generatedRowList[i][1]); //don't forget to add the base
      }
      let sumFixed = sum.toFixed(3);
      // let avg = sum / coupledFiles.length;
      // let avgFixed = +avg.toFixed(3);
      return {
        valueRounded: sumFixed,
        valueFormatted: sumFixed,
        value: sum,
        flotpairs: [[0, sum]],
      };
    } else if (mode === 'table') {
      let tableModel = new TableModel();
      tableModel.columnMap = {};
      tableModel.columns = [];
      tableModel.rows = [];
      tableModel.type = 'table';
      // let tableModel = {
      //   columnMap : {},
      //   columns: [],
      //   rows: [],
      //   type: 'table',
      // };
      let tempTarget = {
        text: '@path',
        filterable: true,
      };
      let tempMetric = {
        text: 'QEEI',
      };
      tableModel.columnMap['@path'] = tempTarget;
      tableModel.columnMap[tempMetric.text] = tempMetric;
      tableModel.columns.push(tempTarget);
      tableModel.columns.push(tempMetric);
      tableModel.rows = _.map(generatedRowList, item => {
        return {
          0: item[0],
          1: item[1],
        };
      });
      let tableList = [];
      tableList.push(tableModel);
      return tableList;
    }
  }

  findRefIndex(columns) {
    let tempRefId = DetangleSrv.getIndex(issueIdColumn, columns);
    if (tempRefId === -1) {
      tempRefId = DetangleSrv.getIndex(authorColumn, columns);
      if (tempRefId === -1) {
        tempRefId = DetangleSrv.getIndex(intervalColumn, columns);
      }
    }
    return tempRefId;
  }

  getCustomConfig(templateSrv, config) {
    config.sourceTypeData = templateSrv.replaceWithText(config.sourceType, null);
    if (config.sourceTypeData === '$issue_type') {
      config.sourceTypeData = 'All';
    }
    config.targetTypeData = templateSrv.replaceWithText(config.targetType, null);
    if (config.targetTypeData === '$target_issue_type') {
      config.targetTypeData = 'All';
    }
    config.authorData = templateSrv.replaceWithText(config.author, null);
    config.issueTitleData = templateSrv.replaceWithText(config.issueTitle, null);
    config.metricRangeData = templateSrv.replaceWithText('$metric_range', null);
    // if (!config.metricRangeData) {
    //   config.metricRangeData = templateSrv.replaceWithText('$metric_range', null);
    // }
    config.fileGroupData = templateSrv.replaceWithText('$file_group', null);

    config.fileExcludeFilterData = templateSrv.replaceWithText('$file_exclude', null);
    if (config.minIssuesPerFile) {
      config.minIssuesData = templateSrv.replaceWithText(config.minIssuesPerFile, null);
    }
    if (_.isEmpty(config.minIssuesData)) {
      config.minIssuesData = '-';
    }
    if (config.minFilesPerIssue) {
      config.minFilesData = templateSrv.replaceWithText(config.minFilesPerIssue, null);
    }
    if (_.isEmpty(config.minFilesData)) {
      config.minFilesData = '-';
    }
    config.fileHighlightData = templateSrv.replaceWithText('$file_highlight', null);
    config.fileIncludeFilterData = templateSrv.replaceWithText('$file_include', null);
    config.variationLevel = templateSrv.replaceWithText('$variation_level', null);
    config.minVariationLevel = templateSrv.replaceWithText('$min_variation_level', null);

    config.minNumberOfCommonRefs = parseInt(templateSrv.replaceWithText('$min_common_connections', null), 10) || 0;
    config.minNumberOfDiffRefs = parseInt(templateSrv.replaceWithText('$min_different_connections', null), 10) || 0;
    config.pathDifferenceLevel = parseInt(templateSrv.replaceWithText('$path_difference_level', null), 10) || 0;
    config.folderLevel = templateSrv.replaceWithText('$folder_level', null);
    if (config.folderLevelData) {
      config.folderLevel = config.folderLevelData;
    }
    config.eachShouldHaveDiff = true;
    return config;
  }

  dataFilter(dataList, config) {
    let t0 = performance.now();

    let getNthIndex = (word, substring, index) => {
      return word.split(substring, index).join(substring).length;
    };
    for (let i = 0; i < dataList.length; i++) {
      let tempColumns = dataList[i].columns;
      let tempRows = dataList[i].rows;
      let tempFilePathIndex: number = DetangleSrv.getIndex(pathColumn, tempColumns);
      let tempIssueIdIndex: number = DetangleSrv.getIndex(issueIdColumn, tempColumns);
      if (tempIssueIdIndex === -1) {
        tempIssueIdIndex = DetangleSrv.getIndex(authorColumn, tempColumns);
        if (tempIssueIdIndex === -1) {
          tempIssueIdIndex = DetangleSrv.getIndex(intervalColumn, tempColumns);
        }
        if (tempIssueIdIndex === -1) {
          tempIssueIdIndex = DetangleSrv.getIndex(daysColumn, tempColumns);
        }
      }
      let tempValueIndex: number = tempColumns.length - 1;
      if (tempFilePathIndex === -1) {
        continue;
      }

      let fileGroup = DetangleSrv.trimConfParameter(config.fileGroupData);
      let shouldGroupFiles =
        fileGroup !== '' && fileGroup !== '-' && fileGroup !== '$file_group' && !config.folderLevelData;
      let isFileGroupInt = DetangleSrv.isInt(fileGroup);
      let fileGroupIndex: number;
      if (shouldGroupFiles) {
        if (isFileGroupInt) {
          fileGroupIndex = parseInt(fileGroup, 10);
          let returnArray = tempRows.filter(
            item => (item[tempFilePathIndex].match(/\//g) || []).length === fileGroupIndex
          );
          for (let i = fileGroupIndex - 1; i > 0; i--) {
            let filteredArray = tempRows.filter(item => (item[tempFilePathIndex].match(/\//g) || []).length === i);
            for (let j = 0; j < filteredArray.length; j++) {
              let tempFilteredObject = filteredArray[j];
              let shouldDirectoryAdded = !returnArray.some(
                item =>
                  item[tempIssueIdIndex] === tempFilteredObject[tempIssueIdIndex] &&
                  item[tempFilePathIndex].includes(tempFilteredObject[tempFilePathIndex])
              );
              if (shouldDirectoryAdded) {
                returnArray.push(tempFilteredObject);
              }
            }
          }
          tempRows = returnArray;
        } else {
          tempRows = tempRows.filter(item => item[tempFilePathIndex].match(fileGroup));
        }
      }

      let folderLevel = DetangleSrv.trimConfParameter(config.folderLevel);
      let shouldApplyFolderLevel = folderLevel !== '' && folderLevel !== '-' && folderLevel !== '$folder_level';
      let isFolderLevelInt = DetangleSrv.isInt(folderLevel);

      if (
        shouldApplyFolderLevel &&
        config.applyFolderLevel &&
        isFolderLevelInt &&
        (!config.coupling || config.metric === 'cohesion')
      ) {
        let folderLevelIndex = parseInt(folderLevel, 10);
        tempRows = tempRows.filter(item => (item[tempFilePathIndex].match(/\//g) || []).length === folderLevelIndex);
      }

      let fileInclusionFilter = DetangleSrv.trimConfParameter(config.fileIncludeFilterData);
      let shouldApplyFileInclusion =
        fileInclusionFilter !== '' && fileInclusionFilter !== '-' && fileInclusionFilter !== '$file_include';
      if (shouldApplyFileInclusion) {
        let regexChecker = new RegExp(fileInclusionFilter);
        tempRows = tempRows.filter(item => regexChecker.test(item[tempFilePathIndex]));
      }

      let fileRegexFilter = DetangleSrv.trimConfParameter(config.fileExcludeFilterData);
      let shouldFilterFiles = fileRegexFilter !== '' && fileRegexFilter !== '-' && fileRegexFilter !== '$file_exclude';
      if (shouldFilterFiles) {
        let regexChecker = new RegExp(fileRegexFilter);
        tempRows = tempRows.filter(item => !regexChecker.test(item[tempFilePathIndex]));
      }

      let metricFilter = DetangleSrv.trimConfParameter(config.metricRangeData);
      let shouldFilterMetric = metricFilter !== '' && metricFilter !== '-' && metricFilter !== '$metric_range';
      if (shouldFilterMetric) {
        let metricFilterArray = metricFilter.split('-');
        if (metricFilterArray.length === 2) {
          let min = metricFilterArray[0];
          let max = metricFilterArray[1];
          tempRows = tempRows.filter(item => item[tempValueIndex] >= min && item[tempValueIndex] <= max);
        } else if (metricFilterArray.length === 1) {
          let min = metricFilterArray[0];
          tempRows = tempRows.filter(item => item[tempValueIndex] >= min);
        }
      }

      let fileVariationFilter = DetangleSrv.trimConfParameter(config.variationLevel);
      let shouldApplyVariationFilter =
        fileVariationFilter !== '' && fileVariationFilter !== '-' && fileVariationFilter !== '$variation_level';
      let minFileVariation = DetangleSrv.trimConfParameter(config.minVariationLevel);
      let shouldApplyMinVariation =
        minFileVariation !== '' && minFileVariation !== '-' && minFileVariation !== '$min_variation_level';

      if (shouldApplyMinVariation && DetangleSrv.isInt(minFileVariation)) {
        let minVariationLevel = parseInt(minFileVariation, 10);
        let rowsVariated;
        if (shouldApplyVariationFilter && DetangleSrv.isInt(fileVariationFilter)) {
          let variationLevel = parseInt(fileVariationFilter, 10);
          rowsVariated = tempRows.map(x => {
            let originalPath = x[tempFilePathIndex];
            let changedParam = {};
            changedParam[tempFilePathIndex] = x[tempFilePathIndex].substring(
              0,
              getNthIndex(originalPath, '/', variationLevel)
            );
            return Object.assign({}, x, changedParam);
          });
        } else {
          rowsVariated = tempRows;
        }
        let issueGroupedArray = DetangleSrv.groupByArray(rowsVariated, tempIssueIdIndex);
        let acceptedIssues = {};
        for (let i = 0; i < issueGroupedArray.length; i++) {
          let tempIssueId = issueGroupedArray[i].item;
          let count = issueGroupedArray[i].connections.reduce(
            function(values, v) {
              if (!values.set[v[tempFilePathIndex]]) {
                values.set[v[tempFilePathIndex]] = 1;
                values.count++;
              }
              return values;
            },
            { set: {}, count: 0 }
          ).count;
          if (count >= minVariationLevel) {
            acceptedIssues[tempIssueId] = true;
          }
        }

        tempRows = tempRows.filter(item => acceptedIssues[item[tempIssueIdIndex]]);
      }
      dataList[i].rows = tempRows;
    }
    let t1 = performance.now();
    console.log('Datafilter is took ' + (t1 - t0) + ' milliseconds.');
    return dataList;
  }

  static getIndex(text, columnList) {
    return _.findIndex(columnList, { text: text });
  }

  removeYearGrouping(a) {
    if (!a.length) {
      return [];
    }
    let hashs = {},
      key = '';
    for (let i = 0; i < a.length; i++) {
      key = a[i][this.issueIdIndex] + '~' + a[i][this.filePathIndex];
      hashs[key] = hashs[key] || { Key: key, Elements: [] };
      hashs[key].Elements.push(a[i][this.valueIndex]);
    }
    let out = [],
      keys = Object.keys(hashs);
    for (let j = 0; j < keys.length; j++) {
      let outObj = {};
      let tempKey = keys[j];
      let fields = tempKey.split('~');
      outObj[this.issueIdIndex] = fields[0];
      outObj[this.filePathIndex] = fields[1];
      outObj[this.valueIndex] = hashs[tempKey].Elements.reduce((a, b) => a + b, 0);
      out.push(outObj);
    }
    return out;
  }

  static isInt(value) {
    // tslint:disable-next-line:no-bitwise
    return (
      !isNaN(value) &&
      (function(x) {
        return (x | 0) === x;
      })(parseFloat(value))
    );
  }

  static nthIndex(str, pat, n) {
    let L = str.length,
      i = -1;
    while (n-- && i++ < L) {
      i = str.indexOf(pat, i);
      if (i < 0) {
        break;
      }
    }
    return i;
  }

  static nthIndexAlternative(str, pat, n) {
    let L = str.length,
      i = -1;
    while (n-- && i++ < L) {
      let oldIndex = i - 1;
      i = str.indexOf(pat, i);
      if (i < 0) {
        return oldIndex;
      }
    }
    return i;
  }

  static trimConfParameter(parameter) {
    return parameter ? parameter.trim() : '';
  }

  static groupBy = (xs, key) => {
    return xs.reduce((rv, x) => {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  };
  static groupByArray = (dataToBeGrouped, groupKey) => {
    let groupObjectdata = DetangleSrv.groupBy(dataToBeGrouped, groupKey);
    return Object.keys(groupObjectdata).map(item => {
      return { item, connections: groupObjectdata[item] };
    });
  };
}

coreModule.service('detangleSrv', DetangleSrv);

const detangleSrv = new DetangleSrv();
export default detangleSrv;

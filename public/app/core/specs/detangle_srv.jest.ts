import { describe, beforeEach, it, expect } from 'test/lib/common';
import detangleSrv from 'app/core/services/detangle_srv';
import TableModel from '../table_model';

function couplingFinder(dataList, issueId) {
  let returnItem: any = null;
  dataList.forEach(item => {
    if (issueId === item.target) {
      returnItem = item;
      return;
    }
  });
  return returnItem;
}

function fileCouplingFinder(dataList, file) {
  let returnItem: any = null;
  dataList.forEach(item => {
    if (file === item.target) {
      returnItem = item;
      return;
    }
  });
  return returnItem;
}

let fileData = [
  {
    0: 'FELIX-5354',
    1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/manager/DependencyManager.java',
    2: 47 + 169 + 26,
  },
  {
    0: 'FELIX-5354',
    1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/manager/SingleComponentManager.java',
    2: 184 + 184 + 147,
  },
  {
    0: 'FELIX-5354',
    1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/manager/AbstractComponentManager.java',
    2: 46 + 146 + 75,
  },
  {
    0: 'FELIX-5455',
    1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/manager/DependencyManager.java',
    2: 15 + 7 + 1,
  },
  {
    0: 'FELIX-5455',
    1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/manager/AbstractComponentManager.java',
    2: 11 + 1,
  },
  {
    0: 'FELIX-5455',
    1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/manager/SingleComponentManager.java',
    2: 107 + 41 + 15,
  },
  {
    0: 'FELIX-5618',
    1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/manager/AbstractComponentManager.java',
    2: 32 + 2,
  },
  {
    0: 'FELIX-5618',
    1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/manager/DependencyManager.java',
    2: 5 + 1,
  },
  {
    0: 'FELIX-5618',
    1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/manager/SingleComponentManager.java',
    2: 4 + 4 + 3,
  },
  {
    0: 'FELIX-5618',
    1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/manager/ServiceTracker.java',
    2: 2 + 6 + 1,
  },
  {
    0: 'FELIX-5618',
    1: 'scr/src/main/java/org/apache/felix/scr/impl/manager/AbstractComponentManager.java',
    2: 32 + 2,
  },
  {
    0: 'FELIX-5618',
    1: 'scr/src/main/java/org/apache/felix/scr/impl/manager/DependencyManager.java',
    2: 5 + 1,
  },
  {
    0: 'FELIX-5618',
    1: 'scr/src/main/java/org/apache/felix/scr/impl/manager/SingleComponentManager.java',
    2: 4 + 4 + 3,
  },
  {
    0: 'FELIX-5618',
    1: 'scr/src/main/java/org/apache/felix/scr/impl/manager/ServiceTracker.java',
    2: 2 + 6 + 1,
  },
];

let dataList = [];
let actualData = new TableModel();
actualData.columnMap = {};
actualData.columns = [];
actualData.rows = [];
actualData.type = 'table';
let tempTarget = {
  text: '@issueId',
  filterable: true,
};
let tempFileTarget = {
  text: '@path',
  filterable: true,
};
let tempMetric = {
  text: 'Sum',
};
actualData.columnMap['@issueId'] = tempTarget;
actualData.columnMap['@path'] = tempFileTarget;
actualData.columnMap['Sum'] = tempMetric;
actualData.columns.push(tempTarget);
actualData.columns.push(tempFileTarget);
actualData.columns.push(tempMetric);
actualData.rows.push({
  0: 'FELIX-4297',
  1: 'framework/src/main/java/org/apache/felix/framework/Felix.java',
  2: 70,
});
actualData.rows.push({
  0: 'FELIX-4502',
  1: 'framework/src/main/java/org/apache/felix/framework/Felix.java',
  2: 98,
});
actualData.rows.push({
  0: 'FELIX-4350',
  1: 'scr/src/main/java/org/apache/felix/scr/impl/manager/DependencyManager.java',
  2: 59,
});
actualData.rows.push({
  0: 'FELIX-4502',
  1: 'scr/src/main/java/org/apache/felix/scr/impl/manager/DependencyManager.java',
  2: 268,
});
actualData.rows.push({
  0: 'FELIX-4297',
  1: 'scr/src/main/java/org/apache/felix/scr/impl/manager/DependencyManager.java',
  2: 126,
});

dataList.push(actualData);

let issueTypeData = new TableModel();
issueTypeData.columnMap = {};
issueTypeData.columns = [];
issueTypeData.rows = [];
issueTypeData.type = 'table';
let tempTypeTarget = {
  text: '@issueType',
  filterable: true,
};
issueTypeData.columnMap['@issueId'] = tempTarget;
issueTypeData.columnMap['@issueType'] = tempTypeTarget;
issueTypeData.columnMap['Sum'] = tempMetric;
issueTypeData.columns.push(tempTarget);
issueTypeData.columns.push(tempTypeTarget);

issueTypeData.rows.push({
  0: 'FELIX-4297',
  1: 'Bug',
});
issueTypeData.rows.push({
  0: 'FELIX-4502',
  1: 'Improvement',
});
issueTypeData.rows.push({
  0: 'FELIX-4350',
  1: 'Bug',
});

dataList.push(issueTypeData);

let newVersionDataList = [];
let newActualData = new TableModel();
newActualData.columnMap = {};
newActualData.columns = [];
newActualData.rows = [];
newActualData.type = 'table';
newActualData.columnMap['@issueId'] = tempTarget;
newActualData.columnMap['@path'] = tempFileTarget;
newActualData.columnMap['Sum'] = tempMetric;
newActualData.columns.push(tempTarget);
newActualData.columns.push(tempFileTarget);
newActualData.columns.push(tempMetric);
newVersionDataList.push(newActualData);
newActualData.rows.push({
  0: 'FELIX-3377',
  1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/inject/methods/BaseMethod.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-3377',
  1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/manager/SingleComponentManager.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-4507',
  1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/manager/SingleComponentManager.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-4507',
  1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/xml/XmlHandler.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-4507',
  1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/metadata/ServiceMetadata.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-4507',
  1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/metadata/ComponentMetadata.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-4507',
  1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/metadata/ReferenceMetadata.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-4507',
  1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/manager/ComponentFactoryImpl.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-4506',
  1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/xml/XmlHandler.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-4506',
  1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/manager/ComponentFactoryImpl.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-4506',
  1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/metadata/ServiceMetadata.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-4506',
  1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/metadata/ComponentMetadata.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-4506',
  1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/metadata/ReferenceMetadata.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-3506',
  1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/manager/SingleComponentManager.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-3507',
  1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/manager/DependencyManager.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-3507',
  1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/metadata/ReferenceMetadata.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-4502',
  1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/inject/methods/BaseMethod.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-4502',
  1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/manager/SingleComponentManager.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-4502',
  1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/manager/DependencyManager.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-4502',
  1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/metadata/ComponentMetadata.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-4502',
  1: 'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/manager/AbstractComponentManager.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-4502',
  1: 'framework/src/main/java/org/osgi/framework/VersionRange.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-4502',
  1: 'framework/src/main/java/org/osgi/framework/launch/Framework.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-4502',
  1: 'framework/src/main/java/org/osgi/framework/Bundle.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-4502',
  1: 'framework/src/main/java/org/osgi/framework/BundleContext.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-4502',
  1: 'framework/src/main/java/org/osgi/framework/Version.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-3504',
  1: 'framework/src/main/java/org/osgi/framework/launch/Framework.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-3504',
  1: 'framework/src/main/java/org/osgi/framework/BundleContext.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-3504',
  1: 'framework/src/main/java/org/osgi/framework/Bundle.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-3504',
  1: 'framework/src/main/java/org/osgi/framework/VersionRange.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-3504',
  1: 'framework/src/main/java/org/osgi/framework/BundlePermission.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-3504',
  1: 'framework/src/main/java/org/osgi/framework/hooks/service/EventListenerHook.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-3504',
  1: 'framework/src/main/java/org/osgi/framework/Version.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-2950',
  1: 'framework/src/main/java/org/osgi/framework/hooks/service/EventListenerHook.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-2950',
  1: 'framework/src/main/java/org/osgi/framework/BundlePermission.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-2950',
  1: 'framework/src/main/java/org/osgi/framework/Bundle.java',
  2: 1,
});
newActualData.rows.push({
  0: 'FELIX-2950',
  1: 'framework/src/main/java/org/osgi/framework/BundleContext.java',
  2: 1,
});

describe('Detangle Service should calculate new coupling calculation', function() {
  let config = {};
  beforeEach(function() {
    config = {
      sortingOrder: 'desc',
      minNumberOfCommonRefs: 2,
      minNumberOfDiffRefs: 1,
    };
  });
  it('new coupling calculate', function() {
    let result = detangleSrv.couplingCalculator(newVersionDataList, {}, config, 'timeseries');
    console.log(result);
    let file = fileCouplingFinder(
      result,
      'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/inject/methods/BaseMethod.java'
    );
    expect(file.datapoints[0][0]).toBe(0.7071067811865475);
  });
});

describe('Detangle Service Neighbors', function() {
  let config = {};
  beforeEach(function() {
    config = {
      target: 'issue',
      metric: 'couplecounts',
      sortingOrder: 'desc',
      sourceTypeData: 'All',
      targetTypeData: 'All',
    };
  });
  it('should neighbors 2', function() {
    let result = detangleSrv.dataCalculator(dataList, config);
    let issue = couplingFinder(result, 'FELIX-4502');
    expect(issue.datapoints[0][0]).toBe(2);
  });
});

describe('Detangle Service Issue Coupling Values', function() {
  let config = {};
  beforeEach(function() {
    config = {
      target: 'issue',
      metric: 'coupling',
      sortingOrder: 'desc',
      sourceTypeData: 'All',
      targetTypeData: 'All',
    };
  });
  it('should coupling value 1.9269520005029905', function() {
    let result = detangleSrv.dataCalculator(dataList, config);
    let issue = couplingFinder(result, 'FELIX-4502');

    expect(issue.datapoints[0][0]).toBe(1.9269520005029905);
  });
});

describe('Detangle Service File Coupling Values', function() {
  let config = {};
  beforeEach(function() {
    config = {
      target: 'file',
      metric: 'coupling',
      sortingOrder: 'desc',
      sourceTypeData: 'All',
      targetTypeData: 'All',
    };
    dataList[0].rows = fileData;
  });
  it('should coupling value 1.9269520005029905', function() {
    let result = detangleSrv.dataCalculator(dataList, config);
    let issue = couplingFinder(
      result,
      'osgi-r7/scr/src/main/java/org/apache/felix/scr/impl/manager/AbstractComponentManager.java'
    );
    expect(issue.datapoints[0][0]).toBe(2.5851840097900953);
  });
});

describe('Detangle Service Source Type Filtering', function() {
  let config = {};
  beforeEach(function() {
    config = {
      target: 'issue',
      metric: 'coupling',
      sortingOrder: 'desc',
      sourceTypeData: 'Bug',
      targetTypeData: 'All',
    };
  });
  it('should not include Improvement Type', function() {
    let result = detangleSrv.dataCalculator(dataList, config);
    let issue = couplingFinder(result, 'FELIX-4502');

    expect(issue).toBe(null);
  });
});

describe('Detangle Service Target Type Filtering', function() {
  let config = {};
  beforeEach(function() {
    config = {
      target: 'issue',
      metric: 'coupling',
      sortingOrder: 'desc',
      sourceTypeData: 'All',
      targetTypeData: 'Improvement',
    };
  });
  it('should not find coupling', function() {
    let result = detangleSrv.dataCalculator(dataList, config);
    let issue = couplingFinder(result, 'FELIX-4502');

    expect(issue).toBe(null);
  });
});

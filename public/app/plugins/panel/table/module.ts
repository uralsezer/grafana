import _ from 'lodash';
import $ from 'jquery';
import { MetricsPanelCtrl } from 'app/plugins/sdk';
import { transformDataToTable } from './transformers';
import { tablePanelEditor } from './editor';
import { columnOptionsTab } from './column_options';
import { TableRenderer } from './renderer';

class TablePanelCtrl extends MetricsPanelCtrl {
  static templateUrl = 'module.html';

  pageIndex: number;
  dataRaw: any;
  table: any;
  renderer: any;

  sortingOrder: any = [];
  couplingMetrics: any = [];
  targetSelections: any = [];
  cohesionCalculationMethods: any = [];

  panelDefaults = {
    targets: [{}],
    transform: 'timeseries_to_columns',
    pageSize: null,
    showHeader: true,
    ratio: false,
    styles: [
      {
        type: 'date',
        pattern: 'Time',
        alias: 'Time',
        dateFormat: 'YYYY-MM-DD HH:mm:ss',
      },
      {
        unit: 'short',
        type: 'number',
        alias: '',
        decimals: 2,
        colors: ['rgba(245, 54, 54, 0.9)', 'rgba(237, 129, 40, 0.89)', 'rgba(50, 172, 45, 0.97)'],
        colorMode: null,
        pattern: '/.*/',
        thresholds: [],
      },
    ],
    columns: [],
    scroll: true,
    fontSize: '100%',
    sort: { col: 0, desc: true },
    /**
     * @detangleEdit start
     * @author Ural
     */
    // Detangle Options
    detangle: {
      coupling: false,
      overview: false,
      combined: false,
      maintainabilityIndex: false,
      qualityEffort: false,
      applyFolderLevel: false,
      ratio: false,
      sortingOrder: 'desc',
      limit: null,
      metric: 'coupling',
      sourceType: '$issue_type',
      targetType: '$target_issue_type',
      sourceTypeData: '',
      targetTypeData: '',
      author: '$author',
      authorData: '',
      yearData: '',
      target: 'issue',
      minIssuesPerFile: null,
      minIssuesData: '',
      minFilesPerIssue: null,
      minFilesData: '',
      issueTitle: '$issue_title',
      issueTitleData: '',
      fileExcludeFilter: '$file_exclude',
      fileExcludeFilterData: '',
      metricRange: '$metric_range',
      metricRangeData: '',
      fileGroup: '$file_group',
      fileGroupData: '',
      localFilter: '',
      cohesionCalculationMethod: 'standard',
      additionalMetric: false,
      isAuthorDashboard: false,
    },
    /**
     * @detangleEdit end
     * @author Ural
     */
  };

  /** @ngInject */
  constructor(
    $scope,
    $injector,
    templateSrv,
    private detangleSrv,
    private annotationsSrv,
    private $sanitize,
    private variableSrv
  ) {
    super($scope, $injector);

    this.pageIndex = 0;

    if (this.panel.styles === void 0) {
      this.panel.styles = this.panel.columns;
      this.panel.columns = this.panel.fields;
      delete this.panel.columns;
      delete this.panel.fields;
    }

    _.defaults(this.panel, this.panelDefaults);

    this.events.on('data-received', this.onDataReceived.bind(this));
    this.events.on('data-error', this.onDataError.bind(this));
    this.events.on('data-snapshot-load', this.onDataReceived.bind(this));
    this.events.on('init-edit-mode', this.onInitEditMode.bind(this));
    this.events.on('init-panel-actions', this.onInitPanelActions.bind(this));

    /**
     * @detangleEdit start
     * @author Ural
     */
    this.sortingOrder = [{ text: 'Ascending', value: 'asc' }, { text: 'Descending', value: 'desc' }];

    this.couplingMetrics = [
      { text: 'Coupling Value', value: 'coupling' },
      { text: 'Num. of Couples', value: 'couplecounts' },
      { text: 'Cohesion Value', value: 'cohesion' },
    ];

    this.targetSelections = [{ text: 'Issues|Committers', value: 'issue' }, { text: 'Files', value: 'file' }];

    this.cohesionCalculationMethods = [
      {
        text: 'Standard',
        value: 'standard',
      },
      {
        text: 'Double',
        value: 'double',
      },
    ];
    /**
     * @detangleEdit end
     * @author Ural
     */
  }

  onInitEditMode() {
    this.addEditorTab('Options', tablePanelEditor, 2);
    this.addEditorTab('Column Styles', columnOptionsTab, 3);
    this.addEditorTab('Detangle', 'public/app/plugins/panel/table/detangle.html', 5);
  }

  onInitPanelActions(actions) {
    actions.push({ text: 'Export CSV', click: 'ctrl.exportCsv()' });
  }

  issueQueries(datasource) {
    this.pageIndex = 0;

    if (this.panel.transform === 'annotations') {
      this.setTimeQueryStart();
      return this.annotationsSrv
        .getAnnotations({
          dashboard: this.dashboard,
          panel: this.panel,
          range: this.range,
        })
        .then(annotations => {
          return { data: annotations };
        });
    }

    return super.issueQueries(datasource);
  }

  onDataError(err) {
    this.dataRaw = [];
    this.render();
  }

  calculateRatio(dataList) {
    if (dataList.lenght < 2) {
      return;
    }
    let pathList = {};
    for (let i = 0; i < dataList.length; i++) {
      // let tempColumns = dataList[i].columns;
      let tempRows = dataList[i].rows;
      let valueIndex = 1;
      for (let rowIndex = 0; rowIndex < tempRows.length; rowIndex++) {
        let tempRow = tempRows[rowIndex];
        let tempPath = tempRow[0];
        let tempValue = tempRow[valueIndex];
        if (!pathList[tempPath]) {
          pathList[tempPath] = {};
        }
        pathList[tempPath]['value' + i] = tempValue;
      }
    }
    let generatedRowList = [];
    Object.keys(pathList).map(item => {
      let tempValueHash = pathList[item];
      let tempRatio = 0;
      if (tempValueHash.value0 && tempValueHash.value1 && tempValueHash.value0 !== 0 && tempValueHash.value1 !== 0) {
        tempRatio = tempValueHash.value0 / tempValueHash.value1;
      }
      generatedRowList.push({
        0: item,
        1: tempRatio,
        2: tempValueHash.value0,
        3: tempValueHash.value1,
      });
    });
    let returnDataList: any = {};
    returnDataList.rows = generatedRowList;
    returnDataList.columns = [
      { text: '@path', filterable: true },
      { text: 'Ratio' },
      { text: 'Metric1' },
      { text: 'Metric2' },
    ];
    returnDataList.columnMap = {
      '@path': { text: '@path', filterable: true },
      Ratio: { text: 'Ratio' },
      Metric1: { text: 'Metric1' },
      Metric2: { text: 'Metric2' },
    };
    returnDataList.type = 'table';
    returnDataList = [returnDataList];
    return returnDataList;
  }

  onDataReceived(dataList) {
    /**
     * @detangleEdit start
     * @author Ural
     */
    if (this.panel.detangle.coupling && !this.panel.detangle.ratio) {
      dataList = this.detangleSrv.dataConvertor(dataList, this.templateSrv, this.panel.detangle);
    } else if (this.panel.detangle.ratio) {
      if (this.panel.detangle.maintainabilityIndex && this.panel.detangle.qualityEffort) {
        let secondDataList = this.detangleSrv.dataConvertor(dataList.slice(1), this.templateSrv, {
          ...this.panel.detangle,
          coupling: false,
          diamondPattern: false,
        });
        dataList = this.detangleSrv.dataConvertor(dataList, this.templateSrv, this.panel.detangle, 'table');
        let newDataList = [];
        newDataList.push(dataList[0]);
        newDataList.push(secondDataList[0]);
        dataList = newDataList;
      } else if (this.panel.detangle.maintainabilityIndex) {
        let secondDataList = this.detangleSrv.dataConvertor(dataList.slice(1), this.templateSrv, {
          ...this.panel.detangle,
          coupling: false,
          diamondPattern: false,
        });
        dataList = this.detangleSrv.dataConvertor(dataList, this.templateSrv, this.panel.detangle, 'table');
        let newDataList = [];
        newDataList.push(secondDataList[0]);
        newDataList.push(dataList[0]);
        dataList = newDataList;
      } else {
        dataList = this.detangleSrv.dataConvertor(dataList, this.templateSrv, this.panel.detangle);
      }
      dataList = this.calculateRatio(dataList);
    }

    if (this.panel.detangle.overview) {
      dataList = this.detangleSrv.getOverview(dataList, this.templateSrv, this.panel.detangle);
    }

    if (this.panel.detangle.combined) {
      let constant = this.detangleSrv.dataConvertor([dataList[0]], this.templateSrv, this.panel.detangle);
      let newDataList = [];
      newDataList.push(constant[0]);
      let constant2 = this.detangleSrv.dataConvertor([dataList[4]], this.templateSrv, this.panel.detangle);
      newDataList.push(constant2[0]);
      let qeeiConfig = _.clone(this.panel.detangle);
      qeeiConfig.qualityEffort = true;
      let qeei = this.detangleSrv.dataConvertor([dataList[1], dataList[2]], this.templateSrv, qeeiConfig);
      qeei[0].columns = qeei[0].columns.slice(0, 2);
      qeei[0].rows = qeei[0].rows.map(item => {
        return { 0: item[0], 1: item[1] };
      });
      newDataList.push(qeei[0]);
      let newConfig = _.clone(this.panel.detangle);
      newConfig.coupling = true;
      newConfig.maintainabilityIndex = true;
      newConfig.diamondPattern = true;
      let couplingResult = this.detangleSrv.dataConvertor(dataList.slice(3), this.templateSrv, newConfig, 'table');
      newDataList.push(couplingResult[0]);
      dataList = combineResults(newDataList);
    }

    function combineResults(dataArray) {
      let pathList = {};
      let nameArray = ['BugIndex', 'FeatureLOC', 'FeatureChangedLOC', 'FeatureDebtIndex'];
      for (let i = 0; i < dataArray.length; i++) {
        let tempColumns = dataArray[i].columns;
        let tempRows = dataArray[i].rows;
        let valueIndex = tempColumns.length - 1;
        for (let rowIndex = 0; rowIndex < tempRows.length; rowIndex++) {
          let tempRow = tempRows[rowIndex];
          let tempPath = tempRow[0];
          let tempValue = tempRow[valueIndex];
          if (!pathList[tempPath]) {
            pathList[tempPath] = {};
          }
          pathList[tempPath][nameArray[i]] = tempValue;
        }
      }
      let generatedRowList = [];
      Object.keys(pathList).map(item => {
        let tempValueHash = pathList[item];
        generatedRowList.push({
          0: item,
          1: tempValueHash.BugIndex || 0,
          2: tempValueHash.FeatureDebtIndex || 0,
          3: tempValueHash.FeatureChangedLOC || 0,
          4: tempValueHash.FeatureLOC || 0,
        });
      });

      let returnDataList: any = {};
      returnDataList.rows = generatedRowList;
      returnDataList.columns = [
        { text: '@path', filterable: true },
        { text: 'BugIndex' },
        { text: 'FeatureDebtIndex' },
        { text: 'FeatureChangedLOC' },
        { text: 'FeatureLOC' },
      ];
      returnDataList.columnMap = {
        '@path': { text: '@path', filterable: true },
        Ratio: { text: 'BugIndex' },
        Metric1: { text: 'FeatureDebtIndex' },
        Metric2: { text: 'FeatureChangedLOC' },
        Metric3: { text: 'FeatureLOC' },
      };
      returnDataList.type = 'table';
      returnDataList = [returnDataList];
      return returnDataList;
    }

    /**
     * @detangleEdit end
     * @author Ural
     */
    this.dataRaw = dataList;
    this.pageIndex = 0;

    // automatically correct transform mode based on data
    if (this.dataRaw && this.dataRaw.length) {
      if (this.dataRaw[0].type === 'table') {
        this.panel.transform = 'table';
      } else {
        if (this.dataRaw[0].type === 'docs') {
          this.panel.transform = 'json';
        } else {
          if (this.panel.transform === 'table' || this.panel.transform === 'json') {
            this.panel.transform = 'timeseries_to_rows';
          }
        }
      }
    }

    this.render();
  }

  render() {
    this.table = transformDataToTable(this.dataRaw, this.panel);
    this.table.sort(this.panel.sort);

    this.renderer = new TableRenderer(
      this.panel,
      this.table,
      this.dashboard.isTimezoneUtc(),
      this.$sanitize,
      this.templateSrv
    );

    return super.render(this.table);
  }

  toggleColumnSort(col, colIndex) {
    // remove sort flag from current column
    if (this.table.columns[this.panel.sort.col]) {
      this.table.columns[this.panel.sort.col].sort = false;
    }

    if (this.panel.sort.col === colIndex) {
      if (this.panel.sort.desc) {
        this.panel.sort.desc = false;
      } else {
        this.panel.sort.col = null;
      }
    } else {
      this.panel.sort.col = colIndex;
      this.panel.sort.desc = true;
    }
    this.render();
  }

  moveQuery(target, direction) {
    super.moveQuery(target, direction);
    super.refresh();
  }

  exportCsv() {
    var scope = this.$scope.$new(true);
    scope.tableData = this.renderer.render_values();
    scope.panel = 'table';
    this.publishAppEvent('show-modal', {
      templateHtml: '<export-data-modal panel="panel" data="tableData"></export-data-modal>',
      scope,
      modalClass: 'modal--narrow',
    });
  }

  link(scope, elem, attrs, ctrl: TablePanelCtrl) {
    var data;
    var panel = ctrl.panel;
    var pageCount = 0;

    function getTableHeight() {
      var panelHeight = ctrl.height;

      if (pageCount > 1) {
        panelHeight -= 26;
      }

      return panelHeight - 31 + 'px';
    }

    function appendTableRows(tbodyElem) {
      ctrl.renderer.setTable(data);
      tbodyElem.empty();
      tbodyElem.html(ctrl.renderer.render(ctrl.pageIndex));
    }

    function switchPage(e) {
      var el = $(e.currentTarget);
      ctrl.pageIndex = parseInt(el.text(), 10) - 1;
      renderPanel();
    }

    function appendPaginationControls(footerElem) {
      footerElem.empty();

      var pageSize = panel.pageSize || 100;
      pageCount = Math.ceil(data.rows.length / pageSize);
      if (pageCount === 1) {
        return;
      }

      var startPage = Math.max(ctrl.pageIndex - 3, 0);
      var endPage = Math.min(pageCount, startPage + 9);

      var paginationList = $('<ul></ul>');

      for (var i = startPage; i < endPage; i++) {
        var activeClass = i === ctrl.pageIndex ? 'active' : '';
        var pageLinkElem = $(
          '<li><a class="table-panel-page-link pointer ' + activeClass + '">' + (i + 1) + '</a></li>'
        );
        paginationList.append(pageLinkElem);
      }

      footerElem.append(paginationList);
    }

    function renderPanel() {
      var panelElem = elem.parents('.panel-content');
      var rootElem = elem.find('.table-panel-scroll');
      var tbodyElem = elem.find('tbody');
      var footerElem = elem.find('.table-panel-footer');

      elem.css({ 'font-size': panel.fontSize });
      panelElem.addClass('table-panel-content');

      appendTableRows(tbodyElem);
      appendPaginationControls(footerElem);

      rootElem.css({ 'max-height': panel.scroll ? getTableHeight() : '' });
    }

    // hook up link tooltips
    elem.tooltip({
      selector: '[data-link-tooltip]',
    });

    function addFilterClicked(e) {
      let filterData = $(e.currentTarget).data();
      var options = {
        datasource: panel.datasource,
        key: data.columns[filterData.column].text,
        value: data.rows[filterData.row][filterData.column],
        operator: filterData.operator,
      };

      ctrl.variableSrv.setAdhocFilter(options);
    }

    elem.on('click', '.table-panel-page-link', switchPage);
    elem.on('click', '.table-panel-filter-link', addFilterClicked);

    var unbindDestroy = scope.$on('$destroy', function() {
      elem.off('click', '.table-panel-page-link');
      elem.off('click', '.table-panel-filter-link');
      unbindDestroy();
    });

    ctrl.events.on('render', function(renderData) {
      data = renderData || data;
      if (data) {
        renderPanel();
      }
      ctrl.renderingCompleted();
    });
  }
}

export { TablePanelCtrl, TablePanelCtrl as PanelCtrl };

import angular from 'angular';
import _ from 'lodash';

export class SubmenuCtrl {
  annotations: any;
  variables: any;
  dashboard: any;
  showAdvanced: boolean;
  showAdvancedButton: boolean;

  /** @ngInject */
  constructor(private $rootScope, private variableSrv, private $location) {
    this.annotations = this.dashboard.templating.list;
    this.variables = this.variableSrv.variables;
    this.showAdvanced = false;
    this.showAdvancedButton = _.some(this.variables, 'advanced', true);
  }

  annotationStateChanged() {
    this.$rootScope.$broadcast('refresh');
  }

  variableUpdated(variable) {
    this.variableSrv.variableUpdated(variable, true);
  }

  openEditView(editview) {
    var search = _.extend(this.$location.search(), { editview: editview });
    this.$location.search(search);
  }
}

export function submenuDirective() {
  return {
    restrict: 'E',
    templateUrl: 'public/app/features/dashboard/submenu/submenu.html',
    controller: SubmenuCtrl,
    bindToController: true,
    controllerAs: 'ctrl',
    scope: {
      dashboard: '=',
    },
  };
}

angular.module('grafana.directives').directive('dashboardSubmenu', submenuDirective);

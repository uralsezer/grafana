import _ from 'lodash';
import coreModule from 'app/core/core_module';

/**
 * @detangleEdit
 * @author Ali
 */
export class DashboardOpenerValueTransformerSrv {
  transform(value) {
    var metricStr = '$metric';

    if (value.endsWith(metricStr)) {
      var stdDev = value.indexOf(' Std Dev');
      if (stdDev !== -1) {
        value = value.substr(0, stdDev);
      } else {
        var valueStrs = value.split(' ');

        //remove last 2
        valueStrs.pop();
        valueStrs.pop();

        value = _.join(valueStrs, ' ');
      }
    }

    return value;
  }
}

coreModule.service('dashboardOpenerValueTransformerSrv', DashboardOpenerValueTransformerSrv);
